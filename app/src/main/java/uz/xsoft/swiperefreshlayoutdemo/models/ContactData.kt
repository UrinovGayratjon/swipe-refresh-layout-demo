package uz.xsoft.swiperefreshlayoutdemo.models

data class ContactData(
    var id: Long,
    var name: String,
    var number: String
)