package uz.xsoft.swiperefreshlayoutdemo.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_contact.view.*
import uz.xsoft.swiperefreshlayoutdemo.R
import uz.xsoft.swiperefreshlayoutdemo.models.ContactData

class ContactAdapter : ListAdapter<ContactData, ContactAdapter.ViewContact>(Diff) {

    companion object {
        val Diff = object : DiffUtil.ItemCallback<ContactData>() {
            override fun areItemsTheSame(oldItem: ContactData, newItem: ContactData): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: ContactData, newItem: ContactData): Boolean {
                return oldItem.name == newItem.name
            }

        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewContact =
        ViewContact(
            LayoutInflater.from(parent.context).inflate(R.layout.item_contact, parent, false)
        )

    override fun onBindViewHolder(holder: ViewContact, position: Int) =
        holder.onBind(getItem(position))

    inner class ViewContact(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind(data: ContactData) {
            itemView.textName.text = data.name
            itemView.textNumber.text = data.number
        }
    }

}