package uz.xsoft.swiperefreshlayoutdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import uz.xsoft.swiperefreshlayoutdemo.adapters.ContactAdapter
import uz.xsoft.swiperefreshlayoutdemo.models.ContactData

class MainActivity : AppCompatActivity() {
    private val contactList = ArrayList<ContactData>()
    private lateinit var adapter: ContactAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = ContactAdapter()
        listView.layoutManager = LinearLayoutManager(this)
        listView.adapter = adapter

        setData()

        swipeRefreshLayout.setOnRefreshListener {
            updateData()
        }

        swipeRefreshLayout.setColorSchemeResources(
            R.color.colorAccent,
            R.color.colorPrimary,
            R.color.colorPrimaryDark
        )
    }

    private fun setData() {
        contactList.add(ContactData(0, "Alijon", "+998945698523"))
        contactList.add(ContactData(1, "Valijon", "+998945698543"))
        contactList.add(ContactData(2, "Azimjon", "+998945698593"))
        contactList.add(ContactData(3, "Mirzohid", "+998945695523"))
        contactList.add(ContactData(4, "Davlatjon", "+998945298523"))
        contactList.add(ContactData(5, "Temur", "+998945698503"))
        contactList.add(ContactData(6, "Ibrohimjon", "+998945188523"))
        contactList.add(ContactData(7, "Doston", "+998945698537"))
        contactList.add(ContactData(8, "Akmaljon", "+998944598523"))
        contactList.add(ContactData(9, "Solijon", "+998945648523"))
        adapter.submitList(contactList)
    }

    private fun updateData() {
        GlobalScope.launch(Dispatchers.IO) {
            delay(2000)
            withContext(Dispatchers.Main) {
                adapter.submitList(contactList.shuffled())
                if (swipeRefreshLayout.isRefreshing) {
                    swipeRefreshLayout.isRefreshing = false
                }
            }
        }
    }
}
